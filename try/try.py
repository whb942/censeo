import torch
import torch.nn as nn
import torch.nn.functional as F
import logging
import sys
import time
import json
import random
import cv2

def get_Cenmodel():
    model = torch.load('/home/wanghaibin/model.pth')
    return model

def predict_nr(img_dst, Cenmodel):
    """
    用于nr模式的预测
    :param dst_path:
    :return:
    """
    try:
        img_dst = torch.from_numpy(img_dst).float()
        mos_pred = Cenmodel(img_dst)
        mos_pred = mos_pred.detach().numpy()
        mos_pred = torch.sigmoid(mos_pred)
        return mos_pred.reshape(-1).tolist()
    except Exception as e:
        logging.error("Error in predict_nr: {}".format(str(e)))
        return None

if __name__=='__main__':
    try:
        model = get_Cenmodel()
        img = cv2.imread('/home/wanghaibin/nfs_89/20230412_4/encoder_after_4000/frame_30566.png')
        #img=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
        print(img.shape)
        result = predict_nr(img, model)
        if result is not None:
            print(result)
        else:
            logging.error("Error in main: predict_nr returned None")
    except Exception as e:
        logging.error("Error in main: {}".format(str(e)))
