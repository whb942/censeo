# -*- coding: utf-8 -*-
# @Time    : 2021/4/12 7:12 下午
# @Author  : shaoguowen
# @Email   : shaoguowen@tencent.com
# @FileName: run.py
# @Software: PyCharm

import os
import argparse
import json
import datetime
import numpy as np
import tensorflow as tf
import cv2
import utils

# config
#config_path = os.path.join('/home/wanghaibin/try', "config.json")
#with open(config_path, "r") as fr:
#    config = json.load(fr)
#    config = utils.Dict2Obj(config)
#config_tf = tf.ConfigProto()
#config_tf.gpu_options.allow_growth = True
#sess = tf.Session(graph=tf.Graph(), config=config_tf)
#tf.saved_model.loader.load(sess, ['serve'],'/home/wanghaibin/try')

config_path = os.path.join('/home/wanghaibin/try', "config.json")
with open(config_path, "r") as fr:
    config = json.load(fr)
    config = utils.Dict2Obj(config)

config_tf = tf.compat.v1.ConfigProto()
config_tf.gpu_options.allow_growth = False
config_tf.log_device_placement = False
sess = tf.compat.v1.Session(graph=tf.Graph(), config=config_tf)
tf.saved_model.loader.load(sess, [tf.saved_model.SERVING], '/home/wanghaibin/try')
def predict_nr(dst_path):
    """
    用于nr模式的预测
    :param dst_path:
    :return:
    """
    if utils.is_img(dst_path):
        img_dst = cv2.imread(dst_path)
        img_dst = utils.transform(img_dst, config.input_process)[np.newaxis, ...]
        img_input = img_dst
        mos_pred = sess.run('test_output:0', feed_dict={'test_input:0': img_input})
        if config.output_process.use_sigmoid:
            mos_pred = utils.sigmoid(mos_pred)
        mos_pred = mos_pred * config.output_process.scale
        return mos_pred.reshape(-1).tolist()


if __name__ == '__main__':
    # result dir

    # model inference

    print(predict_nr('/home/wanghaibin/nfs_89/20230411_1/encoder_after_4000/frame_0.png')) # 0.69
    print(predict_nr('/home/wanghaibin/nfs_89/20230411_1/encoder_after_4000_1200p_nogtsharp/frame_0.png')) #0.88
    print(predict_nr('/home/wanghaibin/nfs_89/20230411_1/encoder_after_5000/frame_0.png'))#0.70
    print(predict_nr('/home/wanghaibin/nfs_89/20230411_1/encoder_after_5000_1200p_nogtsharp/frame_0.png'))#0.89
    print(predict_nr('/home/wanghaibin/nfs_89/20230411_1/encoder_after_6000/frame_0.png'))#0.73
    print(predict_nr('/home/wanghaibin/nfs_89/20230411_1/encoder_after_6000_1200p_nogtsharp/frame_0.png'))#0.91
    print(predict_nr('/home/wanghaibin/nfs_89/20230411_1/encoder_after_7000/frame_0.png'))#0.74
    print(predict_nr('/home/wanghaibin/nfs_89/20230411_1/encoder_after_7000_1200p_nogtsharp/frame_0.png'))#0.92
    sess.close()
